import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
adddetails={
  firstName:"",
  lastName:"",
  mobileNo:"",
  emailId:"",
  bank:"",
  accountNo:"",
  city:""
}
status : string

bank =[
  {
    val : "Bank Of India"
  },
  {
    val : "Bank Of Maharashtra"
  }
]

add(){
  if (!this.adddetails.firstName)
  {
    this.status = "Enter First Name"; 
  }
  else if (!this.adddetails.lastName)
  {
    this.status = "Enter Last Name";   
  }
  else if (!/^[1-9]{1}[0-9]{9}$/.test("" + this.adddetails.mobileNo))
  {
    this.status = "Enter 10 Digit Mobile Number";
  }
  else if (!(/^\w+([\.-]?\w+)@\w+([\.-]?\w+)(\.\w{2,3})+$/.test(this.adddetails.emailId)))
  {
    this.status = "Enter Valid Email Id"; 
  }
  else if (!this.adddetails.bank)
  {
    this.status = "Select Bank";
  }
  else if (!this.adddetails.accountNo)
  {
    this.status = "Enter Account Number";
  }
  else if (!this.adddetails.city)
  {
    this.status = "Enter City";
  }
  else{
    localStorage.setItem("user", JSON.stringify(this.adddetails))
    this.router.navigateByUrl('/dashboard');
  }
}



}
