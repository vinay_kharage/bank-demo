import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    this.getDetails();
  }

  userName: string
  password: string
  pass: string
  user: string
  status: boolean

  getDetails() {
    let u = localStorage.getItem("user")
    try {
      u = JSON.parse(u)
      u = JSON.parse(u)
    } catch (error) {

    }
    if (u) {
      this.user = u["userName"]
      this.pass = u["password"]
      console.log("username, password>>", this.user, this.pass)
    } else return null

  }

  login() {
    if (this.userName == this.user && this.password == this.pass) {
      this.status = true
      this.router.navigateByUrl("/dashboard")
    } else {
      this.status = false
    }
  }
}
