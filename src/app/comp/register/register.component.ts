import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  reg = {
    name: "",
    address: "",
    mobileNo: "",
    emailId: "",
    userName: "",
    password: "",
    confirmPassword: "",
    dob: ""
  }
  status: boolean

  register() {
    if (this.reg.password != this.reg.confirmPassword) {
      this.status = false;
      return
    }
    else {
      this.status = true;
      localStorage.setItem("user", JSON.stringify(this.reg))
      this.router.navigateByUrl('/login');
    }
  }

}
